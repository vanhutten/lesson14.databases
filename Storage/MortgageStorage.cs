﻿using System.Collections.Generic;
using System.Linq;

using Lesson14.Databases.Entities;

using NHibernate;

namespace Lesson14.Databases.Storage
{
    class MortgagesStorage
    {
        private readonly ISessionFactory sessionFactory;

        public MortgagesStorage(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }

        public List<MortgageEntity> GetAll()
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<MortgageEntity>()
                        .OrderBy(it => it.CreatedAt)
                        .ToList();
                }
            }
        }

        public void SaveOrUpdate(MortgageEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entity);
                    tx.Commit();
                }
            }
        }

        public void Save(MortgageEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Save(entity);
                    tx.Commit();
                }
            }
        }

        public void SaveAll(IEnumerable<MortgageEntity> entities)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    foreach (var entity in entities)
                    {
                        session.Save(entity);
                    }
                    tx.Commit();
                }
            }
        }
    }
}
