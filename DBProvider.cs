﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Lesson14.Databases.Cfg;
using Lesson14.Databases.Entities;
using Lesson14.Databases.Storage;
using NHibernate;
using Ncfg = NHibernate.Cfg;
using NHibernate.Mapping.Attributes;
using NHibernate.Tool.hbm2ddl;

using Npgsql;
using NHibernate.Cfg;

namespace Lesson14.Databases
{
    public static class DBProvider
    {
        const string connectionString = "Host=localhost;Username=postgres;Password=2222221;Database=test";
        private static Ncfg.Configuration _configuration;


        public static void ValidateSchema()
        {
            if (_configuration != null)
            {
                try
                {
                    new SchemaValidator(_configuration).Validate();
                }
                catch
                {
                    new SchemaExport(_configuration).Create(false, true);
                }
            }
        }

        public static void InsertClient(string _firstName, string _lastName, string _middleName, string _email)
        {
            var storage = new ClientsStorage(SessionFactory);
            var client = new ClientEntity
            {
                FirstName = _firstName,
                LastName = _lastName,
                MiddleName = _middleName,
                Email = _email
            };
            storage.SaveOrUpdate(client);
            Console.WriteLine($"Insert into CLIENTS table");

            var clients = storage.GetAllWithMiddleName();

            Console.WriteLine($"Returned clients = {string.Join(',', clients.Select(it => it.FirstName))}");
        }

        public static void InsertDeposit(ClientEntity client)
        {
            var depositsStorage = new DepositsStorage(SessionFactory);
            var deposit = new DepositEntity()
            {
                Client = client,
                CreatedAt = DateTime.Now
            };
            depositsStorage.SaveOrUpdate(deposit);
            Console.WriteLine($"Insert into DEPOSITS table");

            var deposits = depositsStorage.GetAll();

            Console.WriteLine($"Returned deposits = {string.Join(',', deposits.Select(it => it.CreatedAt))}");
        }

        public static void InsertMortgage(ClientEntity client, DateTime term)
        {
            var mortgageStorage = new MortgagesStorage(SessionFactory);
            var mortgage = new MortgageEntity()
            {
                Client = client,
                CreatedAt = DateTime.Now,
                Term = term
            };
            mortgageStorage.SaveOrUpdate(mortgage);
            Console.WriteLine($"Insert into MORTGAGE table");

            var mortgages = mortgageStorage.GetAll();

            Console.WriteLine($"Returned mortgage = {string.Join(',', mortgages.Select(it => it.Term))}");
        }

        public static void InsertAllTables(string _firstName, string _lastName, string _middleName, string _email, DateTime term)
        {
            var clientsStorage = new ClientsStorage(SessionFactory);
            var CreationTime = DateTime.Now;
            var client = new ClientEntity
            {
                FirstName = _firstName,
                LastName = _lastName,
                MiddleName = _middleName,
                Email = _email
            };
            clientsStorage.SaveOrUpdate(client);
            Console.WriteLine($"NHibernate Insert into CLIENTS table");

            var depositsStorage = new DepositsStorage(SessionFactory);
            var deposit = new DepositEntity()
            {
                Client = client,
                CreatedAt = CreationTime
            };
            depositsStorage.SaveOrUpdate(deposit);
            Console.WriteLine($"NHibernate Insert into DEPOSITS table");

            var deposits = depositsStorage.GetAll();

            Console.WriteLine($"Returned deposits = {string.Join(',', deposits.Select(it => it.CreatedAt))}");

            var mortgageStorage = new MortgagesStorage(SessionFactory);
            var mortgage = new MortgageEntity()
            {
                Client = client,
                CreatedAt = CreationTime,
                Term = term
            };
            mortgageStorage.SaveOrUpdate(mortgage);
            Console.WriteLine($"NHibernate Insert into MORTGAGE table");

            var mortgages = mortgageStorage.GetAll();

            Console.WriteLine($"Returned mortgage = {string.Join(',', mortgages.Select(it => it.CreatedAt))}");
        }

    
        public static void JoinNHibernate()
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    // обычно не требуется
                    var result = (from c in session.Query<ClientEntity>()
                                  join d in session.Query<DepositEntity>() on c.Id equals d.Client.Id 
                                  select new
                                  {
                                      Email = c.Email,
                                      CreatedAt = d.CreatedAt
                                  }
                                 ).ToList();


                    Console.WriteLine(@$"Returned info = {string.Join(',',
                        result.Select(it => it.Email + ":" + it.CreatedAt)
                        )}");
                }
            }
        }

        public static void ShowClients()
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var result = session.Query<ClientEntity>().OrderBy(c=>c.Id).ToList();
                    foreach (var item in result)
                    {
                        Console.WriteLine(@$"Table Clients = Имя: {item.FirstName}, Фамилия: {item.LastName}, Отчество: {item.MiddleName}, Email: {item.Email}");
                    }
                }
            }
        }

        public static void ShowDeposits()
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var result = (from c in session.Query<ClientEntity>()
                                  join d in session.Query<DepositEntity>() on c.Id equals d.Client.Id
                                  select new
                                  {
                                      Email = c.Email,
                                      CreatedAt = d.CreatedAt
                                  }
                                ).ToList();

                    foreach (var item in result)
                    {
                        Console.WriteLine(@$"Table Deposit = Email: {item.Email}, Время создания: {item.CreatedAt}");
                    }
                }
            }
        }

        public static void ShowMortgages()
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var result = (from c in session.Query<ClientEntity>()
                                  join d in session.Query<MortgageEntity>() on c.Id equals d.Client.Id
                                  select new
                                  {
                                      Email = c.Email,
                                      CreatedAt = d.CreatedAt,
                                      Term = d.Term
                                  }
                                ).ToList();

                    foreach (var item in result)
                    {
                        Console.WriteLine(@$"Table Mortgage = Email: {item.Email}, Время создания: {item.CreatedAt}, Срок погашения кредита: {item.Term} ");
                    }
                }
            }
        }

        public static void GroupByNHibernate()
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var result = session.Query<ClientEntity>()
                        .GroupBy(c => c.FirstName)
                        .Select(g => new
                        {
                            FirstName = g.Key,
                            ClientsCount = g.Count()
                        })
                        //HAVING: .Where(g => g.ClientsCount > 0)
                        .ToList();

                    Console.WriteLine(@$"Returned info = {string.Join("; ",
                        result.Select(it => it.FirstName + ":" + it.ClientsCount.ToString())
                        )}");
                }
            }
        }

        static ISessionFactory sessionFactory;
        static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    var configProperties = new Dictionary<string, string>
                    {
                        { NHibernate.Cfg.Environment.ConnectionDriver, typeof (NHibernate.Driver.NpgsqlDriver).FullName },
                        { NHibernate.Cfg.Environment.Dialect, typeof (NHibernate.Dialect.PostgreSQL82Dialect).FullName },
                        { NHibernate.Cfg.Environment.ConnectionString, connectionString },
                    };

                    var serializer = HbmSerializer.Default;
                    serializer.Validate = true;

                    _configuration = new Configuration()
                        .SetNamingStrategy(new PostgresNamingStrategy())
                        .SetProperties(configProperties)
                        .AddInputStream(serializer.Serialize(Assembly.GetExecutingAssembly()));
                    //   .SetInterceptor(new SqlDebugOutputInterceptor());

                    new SchemaUpdate(_configuration).Execute(true, true);

                    sessionFactory = _configuration.BuildSessionFactory();
                }
                return sessionFactory;
            }
        }
    }
}
