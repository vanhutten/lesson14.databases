﻿using System;

namespace Lesson14.Databases
{
    class Program
    {
        static void Main(string[] args)
        {
            DBProvider.ValidateSchema();
            DBProvider.InsertAllTables("Иван", "Иванов", "Иванович", "ivan@sillymail.ru", new DateTime(2029, 10, 18));
            DBProvider.InsertAllTables("Петр", "Петров", "Петрович", "petr@sillymail.ru", new DateTime(2022, 5, 6));
            DBProvider.InsertAllTables("Сидр", "Сидоров", "Сидорович", "sidr@sillymail.ru", new DateTime(2039,11, 9));
            DBProvider.InsertAllTables("Виниамин", "Виниаминов", "Виниаминович", "vinya@sillymail.ru", new DateTime(2026, 3, 14));
            DBProvider.InsertAllTables("Николай", "Николаев", "Николаевич", "kolyan2000@sillymail.ru", new DateTime(2028, 6, 22));
            Console.WriteLine("Смотрим таблички");
            DBProvider.ShowClients();
            DBProvider.ShowDeposits();
            DBProvider.ShowMortgages();
            Console.WriteLine("Добавить запись в таблицу? Y/N");
            var key = Console.ReadKey();
            Console.WriteLine();
            if (key.Key == ConsoleKey.Y)
            {
                Console.WriteLine("Введите Имя клиента:");
                var firstname = Console.ReadLine();
                Console.WriteLine("Введите Фамилию клиента:");
                var lastname = Console.ReadLine();
                Console.WriteLine("Введите Отчество клиента:");
                var middlename = Console.ReadLine();
                Console.WriteLine("Введите Email клиента:");
                var email = Console.ReadLine();
                DBProvider.InsertClient(firstname, lastname, middlename, email);
            }
        }

    }

}

