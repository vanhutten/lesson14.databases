﻿using System;

using NHibernate.Mapping.Attributes;

namespace Lesson14.Databases.Entities
{
    [Class(Table = "mortgage")]
    public class MortgageEntity
    {
        [Id(0, Name = "Id")]
        [Generator(1, Class = "native")]
        public virtual long Id { get; set; }

        [ManyToOne(Column = "client_id", ForeignKey = "deposits_fk_client_id", Cascade = "all")]
        public virtual ClientEntity Client { get; set; }

        [Property(NotNull = true)]
        public virtual DateTime CreatedAt { get; set; }

        [Property(NotNull = true)]
        public virtual DateTime Term { get; set; }
    }
}
